package im.pwr.edu.pl.lab3;

import im.pwr.edu.pl.lab3.model.Customer;
import im.pwr.edu.pl.lab3.repository.CustomerRepository;
import jdk.jpackage.internal.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
public class Lab3Application {
	Logger log = LoggerFactory.getLogger(Logger.class);

	@RequestMapping("/")
	public String home() {
		return "Hello Docker World";
	}

	@Bean
	public CommandLineRunner dataBootsrap(CustomerRepository repository) {
		return (args) -> {
			// save a few customers
			repository.save(new Customer("Jack", "Bauer"));
			repository.save(new Customer("Chloe", "O'Brian"));
			repository.save(new Customer("Kim", "Bauer"));
			repository.save(new Customer("David", "Palmer"));
			repository.save(new Customer("Michelle", "Dessler"));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");
		};
	}
	public static void main(String[] args) {
		SpringApplication.run(Lab3Application.class, args);
	}

}

===============
Instalacja
===============
Poprawne uruchomienie aplikacji
-------------------------------------------------------------------------------------------------
W celu poprawnego uruchomienia aplikacji należy uruchomić Docker compose, a następnie wybrać
odpowiedni kontener o nazwie "Lab3" i wcisnąć "Run". W celu poprawnego działania aplikacji należy
także posiadać bazę danych PostgreSQL.
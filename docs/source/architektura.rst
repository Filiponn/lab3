============
Architektura
============
Diagram klas
-------------

.. uml::

@startuml
class Customer{
-id: Long
-firstName: String
-lastName: String
+toString()
+getters()
+setters()

}

interface CustomerRepository{
~findByLastName(lastName: String: List<Customer>
~findById(id: Long): Customer

}
class CustomerController{
-customerRepository: CustomerRepository
+getCustomers(): Iterable<Customer>
+getUserById(customerId: Long): Customer

}
Customer --> CustomerRepository
CustomerController --> CustomerRepository
@enduml


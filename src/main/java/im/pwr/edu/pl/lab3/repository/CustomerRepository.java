package im.pwr.edu.pl.lab3.repository;

import org.springframework.data.repository.CrudRepository;
import im.pwr.edu.pl.lab3.model.Customer;
import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
    List<Customer> findByLastName(String lastName);

    Customer findById(long id);
}

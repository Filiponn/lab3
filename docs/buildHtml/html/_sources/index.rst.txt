.. lab3 documentation master file, created by
   sphinx-quickstart on Sun Jun  5 22:18:53 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lab3's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   wprowadzenie
   instalacja
   architektura
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

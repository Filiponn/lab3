========================================
Czym jest Docker oraz Docker compose
========================================
Docker
----------------------------------------
Docker jest oprogramowaniem, które służy
do pakowania aplikacji w kontenery wraz z dodatkowymi
bibliotekami, zależnościami potrzebnymi
do poprawnego działania aplikacji w dowolnym środowisku.

Docker compose
----------------------------------------------------------------------------------------------
Docker compose to narzędzie, które pozwala na zarządzenie wieloma kontenerami łącząc
je w jedną całość. Przydaje się np. w sytuacji, gdy chcemy mieć w osobnym kontenerze
całą aplikacje wraz z jej zależnościami oraz osobny kontener dla bazy danych.